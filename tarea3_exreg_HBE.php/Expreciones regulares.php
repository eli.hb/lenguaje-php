<?php
//Realizar una expresión regular que detecte emails correctos.

$str = 'ejemplo@ejemplo.com'; 
$result = preg_match("/^[a-zA-Z0-9.-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,4}$/", $str);
echo $result; 

?></br>
<?php
//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$str = 'HEBE990623HDFRLL00'; //CADENA CORRECTA
$result = preg_match("/^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/", $str);
echo $result; 


?></br>
<?php
//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$str = 'daljwkfjbaoeufbuoawbfuiabfkhabkfaubefiuabefluanfwuiabiawbdwadaffffffffffffffffffffffffffffffffffffffffwwwwwweffffffffffffffffffffffffffffffffffffffffffffffffffffffff'; //CADENA CORRECTA
$result = preg_match("/^[a-zA-Z]{50,}$/", $str);
echo $result; 

?></br>
<?php
//Crea una funcion para escapar los simbolos especiales.
    $str = " 1";
    echo addslashes($str);

    ?></br>
    <?php
//Crear una expresion regular para detectar números decimales.

$str = '111.5555'; //cadena para numeros sin decimal y con decimal 
$result = preg_match("/^\d*\.?\d*$/", $str);
echo $result; 
 


?>
